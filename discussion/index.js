//[SECTION] Create an API Server using Express

//Identify the proper ingredients/materials and components needed to start the project.

	//use the 'required' directive to load the express module/package.
	//express => this will allow us to access methods and functions that will create a server easier.
const express = require('express')
	//create an application using express.
	//express() -> this creates an express/instant application and we will give an identifier for the app that it will produce.
	//In Layman's term, the application will be our server.
const application = express ();

	//identify a virtual port in which to serve the project.
const port = 4000;

	//assign the established connection/server into the designated port.

application.listen(port, () => console.log('server is running on ${port}'));